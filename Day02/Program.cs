﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shared;

namespace Day02
{
  class Program
  {
    static void Main(string[] args)
    {
      FileReader reader = new FileReader();

      List<string> fileLines = reader.ReadStringLines();

      int depth = 0;
      int horPos = 0;
      int aim = 0;
      // algorithm

      foreach (string fileLine in fileLines)
      {
        List<string> splittedLine = fileLine.Split(" ").ToList();
        if (splittedLine.Count == 2 && int.TryParse(splittedLine[1], out int number))
        {
          if (splittedLine[0].Equals("forward", StringComparison.InvariantCultureIgnoreCase))
          {
            horPos += number;
            depth += aim * number;
          }
          else if (splittedLine[0].Equals("down", StringComparison.InvariantCultureIgnoreCase))
          {
            // depth += number;
            aim += number;
          }
          else if (splittedLine[0].Equals("up", StringComparison.InvariantCultureIgnoreCase))
          {
            // depth -= number;
            aim -= number;
          }
        }
      }

      Console.WriteLine($"Result 1: {depth * horPos}");

      int result2 = 0;
      // algorithm
      Console.WriteLine($"Result 1: {result2}");
      Console.ReadKey();
    }
  }
}