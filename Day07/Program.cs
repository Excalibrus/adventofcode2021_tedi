﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Shared;

namespace Day07
{
  class Program
  {
    static void Main(string[] args)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();

      FileReader reader = new("input.txt");

      string line = reader.ReadStringLines().FirstOrDefault();

      List<int> positions = line.Split(",").Select(int.Parse).ToList();

      int average = (int) (positions.Sum(x => x) / positions.Count);

      int max = positions.Max();
      int min = positions.Min();
      int step = 0;

      int totalFuel = int.MaxValue;
      int wrongCalcCounter = 0;
      while (average + step < max && average - step > min && wrongCalcCounter < positions.Count / 2)
      {
        int fuelUp = positions.Sum(x =>
        {
          int fuel = x - (average - step);
          if (fuel < 0) fuel *= -1;
          int res = calculate(fuel);
          return res;
        });
        if (fuelUp < totalFuel)
        {
          totalFuel = fuelUp;
        }
        else
        {
          wrongCalcCounter++;
        }
        int fuelDown = positions.Sum(x =>
        {
          int fuel = x - (average + step);
          if (fuel < 0) fuel *= -1;
          int res = calculate(fuel);
          return res;
        });
        if (fuelDown < totalFuel)
        {
          totalFuel = fuelUp;
        }
        else if (step > 0)
        {
          wrongCalcCounter++;
        }
        step++;
      }

      int calculate(int numTo)
      {
        int res = 0;
        for (int i = 0; i <= numTo; i++)
        {
          res += i;
        }
        return res;
      }

      Console.WriteLine($"Result {totalFuel}");

    }
  }
}