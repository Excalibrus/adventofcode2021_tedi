﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Shared;

namespace Day03
{
  class Program
  {
    static void Main(string[] args)
    {
      FileReader reader = new();

      List<string> fileLines = reader.ReadStringLines();

      string gamaRate = string.Empty;
      string epsilonRate = string.Empty;

      for (int i = 0; i < fileLines.First().Length; i++)
      {
        int numberOfZeros = fileLines.Count(x => x[i] == '0');
        int numberOfOnes = fileLines.Count - numberOfZeros;
        gamaRate += numberOfOnes > numberOfZeros ? "1" : "0";
        epsilonRate += numberOfOnes > numberOfZeros ? "0" : "1";
      }

      int gamaRateNumber = Convert.ToInt32(gamaRate, 2);
      int epsilonRateNumber = Convert.ToInt32(epsilonRate, 2);

      int result = gamaRateNumber * epsilonRateNumber;

      Console.WriteLine($"Result: {result}");

      int oxNumber = GetResult(fileLines, true);
      int co2Number = GetResult(fileLines, false);

      int result2 = oxNumber * co2Number;

      Console.WriteLine($"Result: {result2}");

    }

    private static int GetResult(List<string> bits, bool calculateOx)
    {
      for (int i = 0; i < bits.First().Length; i++)
      {
        int numberOfZeros = bits.Count(x => x[i] == '0');
        int numberOfOnes = bits.Count - numberOfZeros;
        if (bits.Count > 1)
        {
          bits = (calculateOx ? numberOfOnes >= numberOfZeros : numberOfOnes < numberOfZeros) ? bits.Where(x => x[i] == '1').ToList() : bits.Where(x => x[i] == '0').ToList();
        }
      }
      return Convert.ToInt32(bits.First(), 2);
    }
  }
}