﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Shared;

namespace Day13
{
  static class Program
  {
    static void Main(string[] args)
    {
      Stopwatch sw = new();
      sw.Start();

      FileReader reader = new("input.txt");

      List<string> lines = reader.ReadStringLines();

      List<(int x, int y)> coordinates = lines
          .Where(x => !x.Contains("fold") && !string.IsNullOrWhiteSpace(x))
          .Select(x => x.Split(","))
          .Select(x => (x: int.Parse(x[1]), y: int.Parse(x[0])))
          .ToList();

      List<(string direction, int number)> instructions = lines.Where(x => x.Contains("fold"))
          .Select(x => x.Split("="))
          .Select(x => (coordinate: x[0].Last().ToString(), number: int.Parse(x[1])))
          .ToList();

      int partOne = 0;
      int partTwo = 0;

      for (int i = 0; i < instructions.Count; i++)
      {
        (string direction, int number) = instructions[i];
        coordinates = coordinates.Fold(direction, number);
        if (i == 0)
        {
          partOne = coordinates.Count;
        }
      }

      partTwo = coordinates.Count;

      coordinates.Draw();

      sw.Stop();
      Console.WriteLine($"Result1: {partOne}, Result 2: {partTwo}, Time: {sw.ElapsedMilliseconds} ms");
    }

    private static List<(int x, int y)> Fold(this List<(int x, int y)> coordinates, string dir, int number)
    {
      if (dir == "y")
      {
        return new List<(int x, int y)>(
                coordinates
                    .Where(x => x.x < number)
                    .Concat(coordinates
                        .Where(x => x.x > number)
                        .Select(x => (x: Math.Abs(x.x - (2 * number)), y: x.y))))
            .Distinct()
            .ToList();
      }
      return new List<(int x, int y)>(
              coordinates
                  .Where(x => x.y < number)
                  .Concat(coordinates
                      .Where(x => x.y > number)
                      .Select(x => (x: x.x, y: Math.Abs(x.y - (2 * number))))))
          .Distinct()
          .ToList();
    }

    private static void Draw(this List<(int x, int y)> coordinates)
    {
      int maxX = coordinates.Max(x => x.x);
      int maxY = coordinates.Max(x => x.y);
      for (int x = 0; x <= maxX; x++)
      {
        for (int y = 0; y <= maxY; y++)
        {
          Console.Write(coordinates.Any(cor => cor.x == x && cor.y == y) ? "#" : " ");
        }
        Console.WriteLine();
      }
    }
  }
}