﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Shared;

namespace Day08
{
  class Program
  {
    static void Main(string[] args)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();

      FileReader reader = new("input.txt");

      List<string> lines = reader.ReadStringLines();


      var partOne = lines.Select(line => line
          .Split("|")[1]
          .Split(" ")
          .Select(x => x.Trim())
          .Where(x => !string.IsNullOrWhiteSpace(x))
          .Select(x => x.Length)
          .Count(x => x is 2 or 3 or 4 or 7)
      ).Sum();

      sw.Stop();

      Console.WriteLine($"Result: {partOne}, Time: {sw.ElapsedMilliseconds} ms");

      sw.Start();
      Dictionary<int, List<int>> dict = new();
      dict.Add(0, new List<int>{ 0, 1, 2, 3, 4, 5 });
      dict.Add(1, new List<int>{ 1, 2 });
      dict.Add(2, new List<int>{ 0, 1, 3, 4, 6 });
      dict.Add(3, new List<int>{ 0, 1, 2, 3, 6 });
      dict.Add(4, new List<int>{ 1, 2, 5, 6 });
      dict.Add(5, new List<int>{ 0, 2, 3, 5, 6 });
      dict.Add(6, new List<int>{ 0, 2, 3, 4, 5, 6 });
      dict.Add(7, new List<int>{ 0, 1, 2 });
      dict.Add(8, new List<int>{ 0, 1, 2, 3, 4, 5, 6 });
      dict.Add(9, new List<int>{ 0, 1, 2, 3, 5, 6 });


      int sum = 0;
      foreach ((List<string> Inputs, List<string> Outputs) in lines
          .Select(x => x.Split(" | "))
          .Select(x => x
              .Select(y => y.Split(" ").ToList())
              .ToList())
          .Select(x => ( Inputs: x[0], Outputs: x[1] )).ToList())
      {
        List<(int position, string charItem)> fixedPositions = new();
        List<string> learningPool = Inputs.Concat(Outputs).ToList();

        Dictionary<int,List<string>> groupedPool = learningPool.GroupBy(x => x.Length).ToDictionary(x => x.Key, x => x.ToList());
        dict.Select(x => learningPool.Where(pool => pool.Length == x.Value.Count).ToList());
        string item1 = learningPool.FirstOrDefault(x => x.Length == dict[1].Count);
        string item4 = learningPool.FirstOrDefault(x => x.Length == dict[4].Count);
        string item7 = learningPool.FirstOrDefault(x => x.Length == dict[7].Count);
        string item8 = learningPool.FirstOrDefault(x => x.Length == dict[8].Count);

        string zeroPosition = item7.FirstOrDefault(x => !item4.Contains(x)).ToString();
        fixedPositions.Add((0, zeroPosition));

        List<string> length5Items = learningPool.Where(x => x.Length == 5).Select(x => string.Concat(x.OrderBy(y => y))).Distinct().ToList();
        List<string> length6Items = learningPool.Where(x => x.Length == 6).Select(x => string.Concat(x.OrderBy(y => y))).Distinct().ToList();



        // finding 9
        string usedFor9igit = string.Empty;
        string digit9Almost = item4 + zeroPosition;
        foreach (string item in length6Items)
        {
          List<char> intersect = item.Intersect(digit9Almost).ToList();
          if (intersect.Count == digit9Almost.Length)
          {
            usedFor9igit = item;
            string position3 = item.Except(intersect).FirstOrDefault().ToString();
            fixedPositions.Add((3, position3));
            length6Items = length6Items.Where(x => x != item).ToList();
            break;
          }
        }


        foreach (string item in length6Items)
        {
          char diffItem = item8.Except(item).FirstOrDefault();
          if (item1.Contains(diffItem)) // found digit 6
          {
            fixedPositions.Add((1, diffItem.ToString()));
            char position2 = item1.Except(diffItem.ToString()).FirstOrDefault();
            fixedPositions.Add((2, position2.ToString()));
          }
          else // found digit 0
          {
            fixedPositions.Add((6, diffItem.ToString()));
          }
        }

        char position4 = item8.Except(usedFor9igit).FirstOrDefault();
        fixedPositions.Add((4, position4.ToString()));

        string currentPositions = string.Concat(fixedPositions.Select(x => x.charItem));

        char position5 = item8.Except(currentPositions).FirstOrDefault();
        fixedPositions.Add((5, position5.ToString()));

        if (fixedPositions.Count != 7 && fixedPositions.Any(x => string.IsNullOrWhiteSpace(x.charItem)))
        {
          throw new Exception("Failed at parsing");
        }

        fixedPositions = fixedPositions.OrderBy(x => x.position).ToList();

        string outputValue = string.Empty;
        foreach (string output in Outputs)
        {
          List<int> positions = new();
          foreach (char c in output)
          {
            (int position, string charItem) = fixedPositions.FirstOrDefault(x => x.charItem == c.ToString());
            positions.Add(position);
          }
          (int key, List<int> value) = dict.FirstOrDefault(x => x.Value.Count == positions.Count && x.Value.All(positions.OrderBy(p => p).Contains));
          outputValue += key.ToString();
        }
        if (!int.TryParse(outputValue, out int result))
        {
          throw new Exception("Could not parse this num");
        }
        sum += result;

        Console.WriteLine(result);
      }

      sw.Stop();

      Console.WriteLine($"Result 2: {sum}, Time: {sw.ElapsedMilliseconds} ms");
    }
  }
}