﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Shared;

namespace Day11
{
  static class Program
  {
    static void Main(string[] args)
    {
      Stopwatch sw = new();
      sw.Start();

      FileReader reader = new("input.txt");

      List<string> lines = reader.ReadStringLines();

      int[,] matrix = new int[lines.Count, lines.Count];

      for (int x = 0; x < lines.Count; x++)
      {
        for (int y = 0; y < lines[x].Length; y++)
        {
          matrix[x, y] = int.Parse(lines[x][y].ToString());
        }
      }

      int numberOfSteps = 100;
      // matrix.Draw();
      int partOne = 0;
      int partTwo = 0;
      for (int step = 1; step <= numberOfSteps || partTwo == 0; step++)
      {
        matrix.IncreaseEnergy();
        matrix.DoFlashStep();
        int numberOfFlashed = matrix.ResetFlashedOctopuses();
        if (partTwo == 0 && numberOfFlashed == lines.Count * lines.Count)
        {
          partTwo = step;
        }
        if (step <= numberOfSteps)
        {
          partOne += numberOfFlashed;
        }

        // matrix.Draw();
        // Console.WriteLine($"Current result: {partOne}");
      }

      sw.Stop();
      Console.WriteLine($"Result1: {partOne}, Result 2: {partTwo}, Time: {sw.ElapsedMilliseconds} ms");
    }

    private static void Draw(this int[,] matrix)
    {
      for (int x = 0; x < matrix.GetLength(0); x++)
      {
        for (int y = 0; y < matrix.GetLength(1); y++)
        {
          if (matrix[x, y] == 0) Console.ForegroundColor = ConsoleColor.Yellow;
          Console.Write(matrix[x,y]);
          if (matrix[x, y] == 0) Console.ResetColor();
        }
        Console.WriteLine();
      }
      Console.WriteLine();
    }

    private static int ResetFlashedOctopuses(this int[,] matrix)
    {
      int counter = 0;
      for (int x = 0; x < matrix.GetLength(0); x++)
      {
        for (int y = 0; y < matrix.GetLength(1); y++)
        {
          if (matrix[x, y] > 9)
          {
            matrix[x, y] = 0;
            counter++;
          }
        }
      }
      return counter;
    }

    private static void DoFlashStep(this int[,] matrix)
    {
      List<(int x, int y)> flashedOctopuses = new();
      for (int x = 0; x < matrix.GetLength(0); x++)
      {
        for (int y = 0; y < matrix.GetLength(1); y++)
        {
          matrix.FlashOctopusAndNeighbours(x, y, flashedOctopuses);
        }
      }
    }

    private static void FlashOctopusAndNeighbours(this int[,] matrix, int x, int y, List<(int x, int y)> flashedOctopuses)
    {
      if (matrix[x, y] > 9 && !flashedOctopuses.Contains((x, y)))
      {
        flashedOctopuses.Add((x,y));
        List<(int x, int y)> allNeighbours = GetAllNeighbours(matrix, x, y);
        allNeighbours.ForEach(neighbour => matrix[neighbour.x, neighbour.y]++);
        foreach ((int x, int y) neighbour in allNeighbours)
        {
          matrix.FlashOctopusAndNeighbours(neighbour.x, neighbour.y, flashedOctopuses);
        }
      }
    }

    private static void IncreaseEnergy(this int[,] matrix)
    {
      for (int x = 0; x < matrix.GetLength(0); x++)
      {
        for (int y = 0; y < matrix.GetLength(1); y++)
        {
          matrix[x, y]++;
        }
      }
    }

    private static List<(int x, int y)> GetAllNeighbours(this int[,] matrix, int x, int y)
    {
      int xLength = matrix.GetLength(0);
      int yLength = matrix.GetLength(1);
      List<(int x, int y)> coordinates = new();
      if (x > 0)
      {
        // left
        coordinates.Add((x - 1, y));
        if (y > 0)
        {
          // left up
          coordinates.Add((x - 1, y - 1));
        }
        // left down
        if(y < yLength - 1)
        {
          coordinates.Add((x - 1, y + 1));
        }
      }
      if (x < xLength - 1)
      {
        // right
        coordinates.Add((x + 1, y));
        if (y > 0)
        {
          // right up
          coordinates.Add((x + 1, y - 1));
        }
        // right down
        if(y < yLength - 1)
        {
          coordinates.Add((x + 1, y + 1));
        }
      }
      if (y > 0)
      {
        // up
        coordinates.Add((x, y - 1));
      }
      if (y < yLength - 1)
      {
        // down
        coordinates.Add((x, y + 1));
      }
      return coordinates;
    }
  }
}