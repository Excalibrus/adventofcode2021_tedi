﻿using System.Diagnostics;
using Shared;

static class Program
{
  private static readonly Dictionary<char, string> HexDictionary = new()
  {
      { '0', "0000" },
      { '1', "0001" },
      { '2', "0010" },
      { '3', "0011" },
      { '4', "0100" },
      { '5', "0101" },
      { '6', "0110" },
      { '7', "0111" },
      { '8', "1000" },
      { '9', "1001" },
      { 'A', "1010" },
      { 'B', "1011" },
      { 'C', "1100" },
      { 'D', "1101" },
      { 'E', "1110" },
      { 'F', "1111" }
  };

  static void Main()
  {
    Stopwatch sw = new();
    sw.Start();

    FileReader reader = new("input.txt");
    string originalCode = string.Join("", reader.ReadStringLines());

    string binaryString = string.Join("", originalCode.Select(x => HexDictionary[x]));

    (int version, long number, _) = binaryString.ReadPackage(1);

    sw.Stop();
    Console.WriteLine($"Part one: {version}, Part two: {number}, Ended in {sw.ElapsedMilliseconds} ms");
  }

  private static (int version, long number, int lastIndex) ReadPackage(this string wholePackage, int depth)
  {
    int globalIndex = -1;
    if (wholePackage.All(x => x.Equals('0') || wholePackage.Length == 0))
    {
      return (-1, -1, globalIndex);
    }
    int version = Convert.ToInt32(wholePackage[..3], 2);
    int id = Convert.ToInt32(wholePackage[3..6], 2);
    if (id == 4)
    {
      Console.WriteLine($"Literal value - depth {depth}");
      (long number, int lastIndex) = wholePackage[6..].ReadLiteralValue();
      return (version, number, lastIndex + 6);
    }
    int lengthTypeId = int.Parse(wholePackage[6].ToString());
    if (lengthTypeId == 0)
    {
      Console.WriteLine($"Operator 0 - depth {depth}");
      int sum = 0;
      int index = 7 + 15;
      int numberOfSubPacketBits = Convert.ToInt32(wholePackage[7..index], 2);
      List<long> packetValues = new();
      while (sum < numberOfSubPacketBits)
      {
        (int packageVersion, long number, int lastIndex) = wholePackage[index..].ReadPackage(depth + 1);
        if (packageVersion == -1) return (version, 0, globalIndex);

        sum += lastIndex;
        index += lastIndex;
        version += packageVersion;
        globalIndex = index;
        packetValues.Add(number);
      }

      return (version, packetValues.CalculateValue(id), globalIndex);
    }
    if (lengthTypeId == 1)
    {
      Console.WriteLine($"Operator 1 - depth {depth}");
      int index = 7 + 11;
      List<long> packetValues = new();
      int numberOfSubPackets = Convert.ToInt32(wholePackage[7..index], 2);
      for (int i = 0; i < numberOfSubPackets; i++)
      {
        (int packageVersion, long number, int lastIndex) = wholePackage[index..].ReadPackage(depth + 1);
        if (packageVersion == -1) return (version, 0, globalIndex);

        index += lastIndex;
        version += packageVersion;
        globalIndex = index;
        packetValues.Add(number);
      }
      return (version, packetValues.CalculateValue(id), globalIndex);
    }
    throw new Exception("Invalid length type id");
  }

  private static long CalculateValue(this List<long> packets, int id)
  {
    return id switch
    {
        0 => packets.Sum(x => x),
        1 => packets.Aggregate((x, y) => x * y),
        2 => packets.Min(),
        3 => packets.Max(),
        5 when packets.Count == 2 => packets[0] > packets[1] ? 1 : 0,
        6 when packets.Count == 2 => packets[0] < packets[1] ? 1 : 0,
        7 when packets.Count == 2 => packets[0] == packets[1] ? 1 : 0,
        _ => 0
    };
  }

  private static (long number, int index) ReadLiteralValue(this string package)
  {
    if (string.IsNullOrWhiteSpace(package))
    {
      return (0, 0);
    }
    int index = 0;
    List<string> packets = new();
    for (int i = 0; i < package.Count(); i += 5)
    {
      string? current = string.Join("", package.Skip(i).Take(5));
      if (string.IsNullOrWhiteSpace(current))
      {
        throw new Exception("Could not get current string");
      }
      packets.Add(current[1..]);
      if (current[0] != '0') continue;
      index = i + 5;
      break;
    }
    string? stringNum = string.Join("", packets);
    if (string.IsNullOrWhiteSpace(stringNum)) throw new Exception("Invalid num");
    long number = Convert.ToInt64(stringNum, 2);
    return (number, index);
  }
}