﻿using System.Diagnostics;
using System.Runtime.Serialization;
using Shared;



static class Program
{
  enum Location
  {
    NotReached = 1,
    TargetReached,
    TargetMissed
  }
  public static (int xMin, int yMin, int xMax, int yMax) TargetArea { get; set; }
  static void Main()
  {
    Stopwatch sw = new();
    sw.Start();

    FileReader reader = new("input.txt");

    List<int> numbers = reader.ReadStringLines()
        .First()
        .Split(",")
        .SelectMany(x => x.Split(".."))
        .Select((x, i) => i % 2 == 0 ? x.Split("=")[1] : x)
        .Select(int.Parse)
        .ToList();

    List<int> yValues = numbers.Skip(2).Take(2).ToList();
    TargetArea = ((numbers.Take(2).Min(), Math.Abs(yValues[0]) > Math.Abs(yValues[1]) ? yValues[1] : yValues[0], numbers.Take(2).Max(), Math.Abs(yValues[0]) > Math.Abs(yValues[1]) ? yValues[0] : yValues[1]));

    (int height, int counter) = FindMaxHeight((0, 0));

    sw.Stop();
    Console.WriteLine($"Part one: {height}, Part two: {counter}, Ended in {sw.ElapsedMilliseconds} ms");
  }

  private static (int height, int counter) FindMaxHeight((int x, int y) initialPosition)
  {
    int maxY = int.MinValue;
    int counter = 0;
    for (int xVelocity = 0; xVelocity < 1000; xVelocity++)
    {
      for (int yVelocity = -1000; yVelocity < 1000; yVelocity++)
      {
        int currentY = SimulatePath(initialPosition, (xVelocity, yVelocity));
        if(currentY > int.MinValue)
        {
          counter++;
        }
        if (currentY > maxY)
        {
          maxY = currentY;
        }
      }
    }
    return (maxY, counter);
  }

  private static int SimulatePath((int x, int y) initialPosition, (int x, int y) velocity)
  {
    Location location = Location.NotReached;
    List<(int x, int y)> path = new() { initialPosition };
    while (location == Location.NotReached)
    {
      (initialPosition, velocity, location) = FindNextLocation(initialPosition, velocity);
      path.Add(initialPosition);
    }
    return location == Location.TargetReached ? path.Max(x => x.y) : int.MinValue;
  }

  private static ((int x, int y) position, (int x, int y) velocity, Location location) FindNextLocation((int x, int y) position, (int x, int y) velocity)
  {
    (int x, int y) nextPosition = (position.x + velocity.x, position.y + velocity.y);
    (int x, int y) nextVelocity = (velocity.x > 0 ? velocity.x - 1 : (velocity.x < 0 ? velocity.x + 1 : 0), velocity.y - 1);

    if (nextPosition.x >= TargetArea.xMin &&
        nextPosition.x <= TargetArea.xMax &&
        nextPosition.y >= TargetArea.yMax &&
        nextPosition.y <= TargetArea.yMin)
    {
      return (nextPosition, nextVelocity, Location.TargetReached);
    }
    if (nextPosition.x > TargetArea.xMax || nextPosition.y < TargetArea.yMax)
    {
      return (nextPosition, nextVelocity, Location.TargetMissed);
    }
    return (nextPosition, nextVelocity, Location.NotReached);
  }
}