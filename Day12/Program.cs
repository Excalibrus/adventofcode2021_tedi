﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Shared;

namespace Day12
{
  static class Program
  {
    static void Main(string[] args)
    {
      Stopwatch sw = new();
      sw.Start();

      FileReader reader = new("input.txt");

      List<string> lines = reader.ReadStringLines();

      List<Cave> caves = lines.Select(x => new Cave(x)).ToList();

      int partOne = caves.FindAllPaths(1);

      int partTwo = caves.FindAllPaths(2);

      sw.Stop();
      Console.WriteLine($"Result1: {partOne}, Result 2: {partTwo}, Time: {sw.ElapsedMilliseconds} ms");
    }

    private static int FindAllPaths(this List<Cave> caves, int part)
    {
      List<List<string>> paths = new();
      int pathCounter = 0;
      paths.AddRange(caves.Where(x => x.IsStart).Select(x => new List<string> { "start", x.GetConnectingItem("start") }).ToList());

      bool found = false;
      while (!found)
      {
        paths = paths.ApplyNewCaves(caves);
        paths = paths.Normalize(part);
        if (paths.Select(x => x.Last()).ToList().All(x => x == "end"))
        {
          found = true;
        }
      }

      // DrawPaths(paths);

      return paths.Count;
    }

    private static List<List<string>> Normalize(this List<List<string>> paths, int part)
    {
      List<List<string>> items = paths.Select(x => string.Join(";", x)).Distinct().Select(x => x.Split(";").ToList()).ToList();
      List<int> indexesToDelete = new();
      for (int index = 0; index < items.Count; index++)
      {
        List<string> item = items[index];
        var groupedLowerChars = item.Where(x => x != "start" && x != "end").Where(x => x.All(char.IsLower)).GroupBy(x => x).ToDictionary(x => x, x => x.Count());
        if (part == 1 && groupedLowerChars.Any(x => x.Value > 1) ||
            (part == 2 && groupedLowerChars.Count(x => x.Value > 1) > 1) ||
            groupedLowerChars.Any(x => x.Value > 2))
        {
          indexesToDelete.Add(index);
        }
      }
      indexesToDelete.OrderByDescending(x => x).ToList().ForEach(x => items.RemoveAt(x));
      return items;
    }

    private static List<List<string>> ApplyNewCaves(this List<List<string>> paths, List<Cave> caves)
    {
      List<List<string>> redonePaths = paths.Where(x => x.Last() == "end").ToList();

      List<int> indexes = new();
      for (int i = 0; i < paths.Count; i++)
      {
        if(paths[i].Last() != "end") indexes.Add(i);
      }
      foreach (int editingIndex in indexes)
      {
        List<string> currentPath = new(paths[editingIndex]);
        string currentFinishingString = currentPath.Last();
        List<Cave> connectingCaves = caves.Where(x => x.ContainsItem(currentFinishingString)).ToList();
        if(connectingCaves.Count == 1)
        {
          string end = connectingCaves.First().IsEnd ? "end" : connectingCaves.First().GetConnectingItem(currentFinishingString);
          currentPath.Add(end);
          redonePaths.Add(currentPath);
        }
        else if(connectingCaves.Count > 1)
        {
          List<string> endsList = connectingCaves.Select(x => x.IsEnd ? "end" : x.GetConnectingItem(currentFinishingString)).ToList();
          foreach (string endItem in endsList)
          {
            List<string> newPath = new(currentPath);
            newPath.Add(endItem);
            redonePaths.Add(newPath);
          }
        }
      }
      return redonePaths;
    }

    private static void DrawPaths(List<List<string>> paths)
    {
      foreach (List<string> path in paths.OrderBy(x => string.Join("", x)))
      {
        Console.WriteLine(string.Join(", ", path));
      }
      Console.WriteLine();
    }
  }
}