using System;
using System.Collections.Generic;
using System.Linq;

namespace Day12
{
  public class Cave
  {
    private string _startText = "start";
    private string _endText = "end";
    public string Part1 { get; set; }
    public string Part2 { get; set; }

    public bool IsStart => Part1.Equals(_startText, StringComparison.InvariantCultureIgnoreCase) || Part2.Equals(_startText, StringComparison.InvariantCultureIgnoreCase);
    public bool IsEnd => Part1.Equals(_endText, StringComparison.InvariantCultureIgnoreCase) || Part2.Equals(_endText, StringComparison.InvariantCultureIgnoreCase);

    public Cave(string line)
    {
      List<string> lineSeparated = line.Split("-").ToList();
      if (lineSeparated.Count != 2) throw new Exception("Line parsing failed");
      Part1 = lineSeparated[0];
      Part2 = lineSeparated[1];
    }

    public bool ContainsItem(string item) => (Part1 == item || Part2 == item) && !IsStart;

    public string GetConnectingItem(string item)
    {
      if (IsEnd) return _endText;
      if (item == Part1) return Part2;
      if (item == Part2) return Part1;
      throw new Exception("Could not find connecting item");
    }
  }
}