using Shared;

namespace Day19;

public class PointCalculation
{
  public Point3d PointFrom { get; set; }
  public Point3d PointTo { get; set; }
  public Point3d Distance { get; set; }
}