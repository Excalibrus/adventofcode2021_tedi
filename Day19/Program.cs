﻿using System.Diagnostics;
using System.Text.RegularExpressions;
using Shared;

namespace Day19;

public static class Program
{
  static void Main()
  {
    Stopwatch sw = new();
    sw.Start();

    FileReader reader = new("input.txt");

    List<string> lines = reader.ReadStringLines();

    List<Scanner> scanners = new();

    Scanner parsingScanner = new();
    foreach (string line in lines)
    {
      if (line.Contains("scanner"))
      {
        if (!int.TryParse(new Regex(@"\d+").Match(line).Value, out int id))
        {
          throw new Exception("Could not parse id from scanner");
        }
        parsingScanner = new Scanner { Id = id };
        scanners.Add(parsingScanner);
      }
      else if (!string.IsNullOrWhiteSpace(line))
      {
        parsingScanner.Points.Add(new Point3d().Parse(line));
      }
    }

    // List<Point3d> results = new FileReader("result.txt")
    //     .ReadStringLines()
    //     .Select(x => new Point3d().Parse(x))
    //     .OrderBy(x => x.X)
    //     .ThenBy(x => x.Y)
    //     .ThenBy(x => x.Z)
    //     .ToList();

    (int beacons, int distance) = scanners.PartOneMergeFromZero();
    // int beacons = scanners.PartOneMergeMultiple();

    sw.Stop();
    Console.WriteLine($"Part one: {beacons}, Part two: {distance}, Ended in {sw.ElapsedMilliseconds} ms");
    Console.ReadKey();
  }

  private static (int beacons, int largestDistance) PartOneMergeFromZero(this List<Scanner> scanners)
  {
    scanners.ForEach(x => x.CalculatePaths());
    Scanner scanner = scanners[0];

    List<Scanner> availableScanners = scanners.Skip(1).ToList();

    List<(int scannerId, Point3d scannerPosition)> scannerPositions = new() { (0, new Point3d(0,0,0)) };

    while (availableScanners.Any())
    {
      Console.WriteLine($"Remaining scanners: {availableScanners.Count}");
      // Console.WriteLine($"Beacons at original: {scanner.Points.Count}");
      // Console.WriteLine($"Paths at original: {scanner.Paths[0].Count}");
      (int scannerId, int rotateDirection, List<(Point3d originalPoint, Point3d relativePoint)> points) = scanner.FindScannerWithSamePoints(availableScanners);
      if (!points.Any())
      {
        throw new Exception("Could not get any points");
      }
      Scanner scannerFound = availableScanners?.FirstOrDefault(x => x.Id == scannerId);

      List<Point3d> pointsToAdd = scannerFound.Points.Except(points.Select(x => x.originalPoint.RotatePoint(rotateDirection))).ToList();
      Point3d calculationPoint = points.First().relativePoint - points.First().originalPoint;
      List<Point3d> point3ds = pointsToAdd.Select(x => x.RotatePoint(rotateDirection) + calculationPoint).ToList();
      scanner.Points.AddRange(point3ds);
      // Console.WriteLine($"Calculating...");
      scanner.CalculatePaths(0);
      // Console.WriteLine($"Calculating finished");
      availableScanners = availableScanners.Where(x => x.Id != scannerId).ToList();
      scannerPositions.Add((scannerId, calculationPoint));
    }
    int maxPosition = 0;
    for (int i = 0; i < scannerPositions.Count; i++)
    {
      for (int j = scannerPositions.Count - 1; j >= 0; j--)
      {
        if (j != i)
        {
          Point3d? scannerPosition = scannerPositions[i].scannerPosition - scannerPositions[j].scannerPosition;
          int positionDistance = Math.Abs(scannerPosition.X) + Math.Abs(scannerPosition.Y) + Math.Abs(scannerPosition.Z);
          if (positionDistance > maxPosition)
          {
            maxPosition = positionDistance;
          }
        }

      }
    }
    return (scanner.Points.Distinct().Count(), maxPosition);
  }

  private static int PartOneMergeMultiple(this List<Scanner> scanners)
  {
    List<Scanner> mergedScanners = scanners.Merge();
    while (mergedScanners.Count > 1)
    {
      mergedScanners = mergedScanners.Merge();
    }
    return mergedScanners[0].Points.Distinct().Count();
  }

  private static List<Scanner> Merge(this List<Scanner> scanners)
  {
    Console.WriteLine($"All scanners: {scanners.Count}");
    List<Scanner> mergedScanners = new();
    List<Scanner> availableScanners = new(scanners);

    while (availableScanners.Count > 0)
    {
      if (availableScanners.Count == 1)
      {
        mergedScanners.AddRange(availableScanners);
        break;
      }
      Scanner firstScanner = availableScanners.First();
      (int scannerId, int rotateDirection, List<(Point3d originalPoint, Point3d relativePoint)> points) = firstScanner.FindScannerWithSamePoints(availableScanners.Where(x => x.Id != firstScanner.Id).ToList());
      if (points.Any())
      {
        Scanner secondScanner = availableScanners?.FirstOrDefault(x => x.Id == scannerId);

        List<Point3d> pointsToAdd = secondScanner.Points.Except(points.Select(x => x.originalPoint.RotatePoint(rotateDirection))).ToList();
        Point3d calculationPoint = points.First().relativePoint - points.First().originalPoint;
        List<Point3d> point3ds = pointsToAdd.Select(x => x.RotatePoint(rotateDirection) + calculationPoint).ToList();
        firstScanner.Points.AddRange(point3ds);
        availableScanners = availableScanners.Where(x => x.Id != secondScanner.Id && x.Id != firstScanner.Id).ToList();
        mergedScanners.Add(firstScanner);
        Console.WriteLine($"Merged scanners {firstScanner.Id} and {secondScanner.Id}");
      }
      else
      {
        mergedScanners.AddRange(availableScanners);
        break;
      }

    }
    return mergedScanners;
  }

  private static (int scannerId, int rotateDirection, List<(Point3d originalPoint, Point3d relativePoint)> points) FindScannerWithSamePoints(this Scanner scanner, List<Scanner> scanners, bool calculatePaths = true)
  {
    if (calculatePaths)
    {
      scanner.CalculatePaths(0);
    }

    foreach (Scanner scanner1 in scanners)
    {
      if (calculatePaths)
      {
        scanner1.CalculatePaths();
      }


      // Console.WriteLine($"Scanner {scanner1.Id}");
      foreach ((int direction, HashSet<int> comparingHash) in scanner1.PathHashes)
      {
        List<int> sameHashes = comparingHash.Where(x => scanner.PathHashes[0].Contains(x)).ToList();
        if (sameHashes.Count < 12)
        {
          continue;
        }
        List<(Point3d originalPoint, Point3d relativePoint)> connectingPoints = new();
        // List<PointCalculation> sameCalculations = pointCalculations.Where(x => scanner.Paths[0].Select(y => y.Distance).Any(y => y.Equals(x.Distance))).ToList();
        // if (sameCalculations.Count >= 12)
        // {
        //   Console.WriteLine("This fast");
        // }
        List<PointCalculation> comparingCalculations = scanner1.Paths[direction].Where(x => sameHashes.Contains(x.Distance.GetHashCode())).ToList();
        List<PointCalculation> originalCalculations = scanner.Paths[0].Where(x => sameHashes.Contains(x.Distance.GetHashCode())).ToList();
        foreach (PointCalculation calculation in originalCalculations)
        {
          PointCalculation? samePointCalculation = comparingCalculations.FirstOrDefault(x => x.Distance.Equals(calculation.Distance));
          if (samePointCalculation != null)
          {
            if (!connectingPoints.Contains((samePointCalculation.PointFrom, calculation.PointFrom)))
            {
              connectingPoints.Add((samePointCalculation.PointFrom, calculation.PointFrom));
            }
            if (!connectingPoints.Contains((samePointCalculation.PointTo, calculation.PointTo)))
            {
              connectingPoints.Add((samePointCalculation.PointTo, calculation.PointTo));
            }
          }
        }
        if (connectingPoints.Count >= 12)
        {
          return (scanner1.Id, direction, connectingPoints);
        }
      }
    }
    return (0, 0, new List<(Point3d originalPoint, Point3d relativePoint)>());
  }
}