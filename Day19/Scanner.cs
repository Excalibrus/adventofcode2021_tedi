using System.Drawing;
using Shared;

namespace Day19;

public class Scanner
{
  public int Id { get; set; }
  public List<Point3d> Points { get; set; } = new();

  public Dictionary<int, List<PointCalculation>> Paths { get; set; } = new();
  public Dictionary<int, HashSet<int>> PathHashes { get; set; } = new();


  public void CalculatePaths(int? direction = null)
  {
    Paths = new Dictionary<int, List<PointCalculation>>();
    PathHashes = new Dictionary<int, HashSet<int>>();

    if (direction.HasValue)
    {
      List<PointCalculation> pointCalculations = CalculatePoints(direction.Value);
      PathHashes.Add(direction.Value, pointCalculations.Select(x => x.Distance.GetHashCode()).ToHashSet());
      Paths.Add(direction.Value, pointCalculations);
    }
    else
    {
      for (int i = 0; i < 24; i++)
      {
        List<PointCalculation> pointCalculations = CalculatePoints(i);
        PathHashes.Add(i, pointCalculations.Select(x => x.Distance.GetHashCode()).ToHashSet());
        Paths.Add(i, pointCalculations);
      }
    }

  }

  private List<PointCalculation> CalculatePoints(int rotation)
  {
    List<Point3d> rotatedPoints = rotation > 0 ? Points.Select(x => x.RotatePoint(rotation)).ToList() : Points;
    List<PointCalculation> points = new();
    for (int i = 0; i < rotatedPoints.Count - 1; i++)
    {
      for (int j = i + 1; j < rotatedPoints.Count; j++)
      {
        points.Add(new PointCalculation
        {
            PointFrom = rotatedPoints[i],
            PointTo = rotatedPoints[j],
            Distance = rotatedPoints[i] - rotatedPoints[j]
        });
        points.Add(new PointCalculation
        {
            PointFrom = rotatedPoints[j],
            PointTo = rotatedPoints[i],
            Distance = rotatedPoints[j] - rotatedPoints[i]
        });
      }
    }
    return points;
  }
}