﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Shared;

namespace Day14
{
  static class Program
  {
    static void Main(string[] args)
    {
      Stopwatch sw = new();
      sw.Start();

      FileReader reader = new("input_demo.txt");
      List<string> lines = reader.ReadStringLines();
      string template = lines.First();

      List<(string text, string insert)> insertionRules = lines.Where((x, i) => i > 1).Select(x => x.Split(" -> ")).Select(x => (text: x[0], insert: x[1])).ToList();


      long partOne = insertionRules.PrecompileRules(10).Calculate(template);
      long partTwo = insertionRules.PrecompileRules(40).Calculate(template);

      sw.Stop();
      Console.WriteLine($"Result1: {partOne}, Result 2: {partTwo}, Time: {sw.ElapsedMilliseconds} ms");
    }

    private static List<PrecompiledRule> PrecompileRules(this List<(string text, string insert)> insertionRules, int steps)
    {
      List<PrecompiledRule> precompiledRules = new();
      for (int i = 0; i < insertionRules.Count; i++)
      {
        (string text, string insert) = insertionRules[i];
        Console.WriteLine($"Precompiling - {text} -> {insert} ({i+1}/{insertionRules.Count})");
        PrecompiledRule precompiledRule = new()
        {
            TwoChars = text,
            PrecompiledText = DoSteps(text, insertionRules, steps / 2)
        };
        precompiledRule.DoCount();
        precompiledRules.Add(precompiledRule);
      }
      return precompiledRules;
    }

    private static long Calculate(this List<PrecompiledRule> precompiledRules, string startingTemplate)
    {
      Dictionary<char, long> sums = precompiledRules.SelectMany(x => x.TwoChars.ToList()).Distinct().ToDictionary(x => x, x => (long)0);

      for (int templateIndex = 0; templateIndex < startingTemplate.Length - 1; templateIndex++)
      {
        PrecompiledRule precompiledRule = precompiledRules.First(x => x.TwoChars == startingTemplate.Substring(templateIndex, 2));

        for (int i1 = 0; i1 < precompiledRule.PrecompiledText.Length - 1; i1++)
        {
          if (i1 % 100000 == 0 && i1 > 0)
          {
            Console.WriteLine($"Working {templateIndex}/{startingTemplate.Length - 2} {Math.Round(i1 / (decimal)precompiledRule.PrecompiledText.Length * 100, 2)} %");
          }
          string templateInnerSub = precompiledRule.PrecompiledText.Substring(i1, 2);
          PrecompiledRule precompiledInnerRule = precompiledRules.First(x => x.TwoChars == templateInnerSub);
          foreach (char key in precompiledInnerRule.PrecompiledCounter.Keys)
          {
            sums[key] += precompiledInnerRule.PrecompiledCounter[key];
          }
          sums[precompiledInnerRule.PrecompiledText[^1]]--;
        }
      }
      sums[startingTemplate[^1]]++;

      List<long> partTwoResults = sums.Values.OrderBy(x => x).ToList();

      return partTwoResults.Last() - partTwoResults.First();
    }

    private static string DoSteps(string template, List<(string text, string insert)> insertionRules, int steps)
    {
      StringBuilder sb = new();
      for (int step = 1; step <= steps; step++)
      {
        for (int i = 0; i < template.Length - 1; i++)
        {
          string currentText = template.Substring(i, 2);
          (string text, string insert) = insertionRules.FirstOrDefault(x => x.text == currentText);
          sb.Append(currentText[0]);
          if (!string.IsNullOrWhiteSpace(text))
          {
            sb.Append(insert);
          }
        }
        sb.Append(template.Last());
        template = sb.ToString();
        sb = new StringBuilder();
      }
      return template;
    }
  }
}