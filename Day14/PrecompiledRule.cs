using System.Collections.Generic;
using System.Linq;

namespace Day14
{
  public class PrecompiledRule
  {
    public string TwoChars { get; set; }
    public string PrecompiledText { get; set; }
    public Dictionary<char, int> PrecompiledCounter { get; set; } = new();

    public void DoCount()
    {
      PrecompiledCounter = PrecompiledText.GroupBy(x => x).ToDictionary(x => x.Key, x => x.Count());
    }
  }
}