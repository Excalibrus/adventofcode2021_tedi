namespace Day04
{
  public class Board
  {
    public BoardNumber[,] Numbers { get; set; } = new BoardNumber[5,5];
    public int FilledLines { get; set; } = 0;
    public int WonResult { get; set; }
    public int WonNumber { get; set; } = 0;

  }
}