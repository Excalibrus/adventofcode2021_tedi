﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shared;

namespace Day04
{
  public class Program
  {
    static void Main(string[] args)
    {
      FileReader reader = new("input.txt");

      List<string> fileLines = reader.ReadStringLines();

      List<int> moves = fileLines.First().Split(",").Select(int.Parse).ToList();
      List<Board> boards = new();
      Board currentBoard = new();
      for (int i = 2; i < fileLines.Count; i++)
      {
        string fileLine = fileLines[i];
        if (string.IsNullOrWhiteSpace(fileLine))
        {
          boards.Add(currentBoard);
          currentBoard = new Board();
        }
        else
        {
          List<int> numbers = fileLine.Split(" ").Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).Select(int.Parse).ToList();
          if (numbers.Count != 5) throw new Exception("Incorrect number of items");
          for (int y = 0; y < numbers.Count; y++)
          {
            currentBoard.Numbers[currentBoard.FilledLines, y] = new BoardNumber {Number = numbers[y]};
          }
          currentBoard.FilledLines++;
          if(i == fileLines.Count - 1) boards.Add(currentBoard);
        }
      }

      foreach (int move in moves)
      {
        foreach (Board board in boards)
        {
          if(board.WonNumber > 0) continue;

          if (ApplyNumberToBoard(board, move))
          {
            if (IsBoardWon(board))
            {
              int result = SumUncheckedNumbers(board) * move;
              board.WonResult = result;
              board.WonNumber = boards.Max(x => x.WonNumber) + 1;
            }
          }
        }
      }

      Console.WriteLine($"Result 1: {boards.FirstOrDefault(x => x.WonNumber == 1)?.WonResult}");
      Console.WriteLine($"Result 2: {boards.FirstOrDefault(x => x.WonNumber == boards.Count)?.WonResult}");

      Console.ReadKey();
    }

    private static bool ApplyNumberToBoard(Board board, int number)
    {
      for (int i = 0; i < board.Numbers.GetLength(0); i++)
      {
        for (int j = 0; j < board.Numbers.GetLength(1); j++)
        {
          if (board.Numbers[i, j].Number == number)
          {
            board.Numbers[i, j].IsChecked = true;
            return true;
          }
        }
      }
      return false;
    }

    private static bool IsBoardWon(Board board)
    {
      // horizontal
      for (int i = 0; i < board.Numbers.GetLength(0); i++)
      {
        for (int j = 0; j < board.Numbers.GetLength(1); j++)
        {
          if (!board.Numbers[i, j].IsChecked) break;
          if (j == board.Numbers.GetLength(1) - 1)
          {
            return true;
          }
        }
      }

      // vertical
      for (int i = 0; i < board.Numbers.GetLength(1); i++)
      {
        for (int j = 0; j < board.Numbers.GetLength(0); j++)
        {
          if (!board.Numbers[j,i].IsChecked) break;
          if (j == board.Numbers.GetLength(0) - 1)
          {
            return true;
          }
        }
      }

      return false;
    }

    private static int SumUncheckedNumbers(Board board)
    {
      int sum = 0;
      for (int i = 0; i < board.Numbers.GetLength(0); i++)
      {
        for (int j = 0; j < board.Numbers.GetLength(1); j++)
        {
          if (!board.Numbers[i, j].IsChecked) sum += board.Numbers[i,j].Number;
        }
      }
      return sum;
    }
  }
}