namespace Day04
{
  public class BoardNumber
  {
    public int Number { get; set; }
    public bool IsChecked { get; set; } = false;
  }
}