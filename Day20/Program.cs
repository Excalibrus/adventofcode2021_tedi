﻿using System.Diagnostics;
using Shared;

static class Program
{
  static void Main()
  {
    Stopwatch sw = new();
    sw.Start();

    FileReader reader = new("input.txt");

    List<string> lines = reader.ReadStringLines();

    bool reversed = false;
    string enhancementAlgorithm = string.Join("", lines.TakeWhile(x => !string.IsNullOrWhiteSpace(x)));
    // if (enhancementAlgorithm.First() == '#')
    // {
    //   reversed = false;
    //   enhancementAlgorithm = string.Join("", enhancementAlgorithm.Select(x => x == '.' ? '#' : '.'));
    // }

    List<string> matrixLines = lines.SkipWhile(x => !string.IsNullOrWhiteSpace(x)).Take(1..).ToList();
    int[,] matrix = new int[matrixLines.Count, matrixLines[0].Length];
    for (int y = 0; y < matrixLines.Count; y++)
    {
      string line = matrixLines[y];
      if (!string.IsNullOrWhiteSpace(line))
      {
        for (int x = 0; x < line.Length; x++)
        {
          if (reversed)
          {
            matrix[y,x] = line[x] == '#' ? 0 : 1;
          }
          else
          {
            matrix[y,x] = line[x] == '#' ? 1 : 0;
          }

        }
      }
    }
    int count = 10;
    for (int i = 0; i < count; i++)
    {
      matrix = matrix.Enhance(enhancementAlgorithm);
    }

    count = 1;
    matrix.Draw(count);

    // hack to count only inner positions (visible in console)
    int num = matrix.AllPositions().Where(x => x.x > count && x.x < matrix.GetLength(0) - count && x.y > count && x.y < matrix.GetLength(1) - count).Count(x => matrix[x.y, x.x] == 1);
    sw.Stop();
    Console.WriteLine($"Part one: {num}, Part two: {0}, Ended in {sw.ElapsedMilliseconds} ms");
    Console.ReadKey();
  }

  private static int[,] Enhance(this int[,] matrix, string algorithm)
  {
    int[,] largeMatrix = matrix.Enlarge(5);
    // largeMatrix.Draw();
    int matrixX = largeMatrix.GetLength(0);
    int matrixY = largeMatrix.GetLength(1);
    int[,] image = new int[matrixX, matrixY];
    for (int y = 0; y < matrixY; y++)
    {
      for (int x = 0; x < matrixX; x++)
      {
        (int x, int y) position = (x, y);
        List<(int x, int y)> coordinates = new List<(int x, int y)?>
        {
            position.GetNeighbourCoordinate(MatrixPosition.TopLeft, matrixX, matrixY),
            position.GetNeighbourCoordinate(MatrixPosition.Top, matrixX, matrixY),
            position.GetNeighbourCoordinate(MatrixPosition.TopRight, matrixX, matrixY),
            position.GetNeighbourCoordinate(MatrixPosition.Left, matrixX, matrixY),
            position,
            position.GetNeighbourCoordinate(MatrixPosition.Right, matrixX, matrixY),
            position.GetNeighbourCoordinate(MatrixPosition.BottomLeft, matrixX, matrixY),
            position.GetNeighbourCoordinate(MatrixPosition.Bottom, matrixX, matrixY),
            position.GetNeighbourCoordinate(MatrixPosition.BottomRight, matrixX, matrixY),
        }.Where(pos => pos.HasValue).Select(pos => pos.Value).ToList();

        if (coordinates.Count == 0)
        {
          continue;
        }

        List<int> neighbours = coordinates.Select(cor => largeMatrix[cor.y, cor.x]).ToList();

        // neighbours.Reverse();
        int index = Convert.ToInt32(string.Join("", neighbours), 2);
        char algorithmChar = algorithm.ElementAt(index);
        image[position.y, position.x] = algorithmChar == '#' ? 1 : 0;
      }
    }

    return image;
  }

  private static int[,] Enlarge(this int[,] matrix, int enlargeFactor)
  {
    int matrixX = matrix.GetLength(0);
    int matrixY = matrix.GetLength(1);
    int[,] newMatrix = new int[matrixX + enlargeFactor * 2, matrixY + enlargeFactor * 2];
    for (int i = 0; i < matrixX + enlargeFactor * 2; i++)
    {
      for (int j = 0; j < matrixY + enlargeFactor * 2; j++)
      {
        if (i < enlargeFactor || i >= matrixX + enlargeFactor || j < enlargeFactor || j >= matrixY + enlargeFactor)
        {
          newMatrix[i, j] = 0;
        }
        else
        {
          newMatrix[i, j] = matrix[i - enlargeFactor, j - enlargeFactor];
        }
      }
    }
    return newMatrix;
  }

  public static void Draw(this int[,] matrix, int trim)
  {
    Console.WriteLine("\n\n");
    for (int y = trim; y < matrix.GetLength(1) - trim; y++)
    {
      for (int x = trim; x < matrix.GetLength(0) - trim; x++)
      {
        Console.Write(matrix[y,x] == 1 ? "#" : ".");
      }
      Console.WriteLine();
    }

  }
}
