namespace Day21;

public class Dice
{
  public int DiceRolls = 0;

  private int _currentDiceRoll = 0;

  public int Roll(int numberOfTimes)
  {
    int move = 0;
    for (int i = 0; i < numberOfTimes; i++)
    {
      _currentDiceRoll++;
      if (_currentDiceRoll == 101)
      {
        _currentDiceRoll = 1;
      }
      move += _currentDiceRoll;
      DiceRolls++;
    }
    return move;
  }

  public override string ToString()
  {
    return DiceRolls.ToString();
  }
}