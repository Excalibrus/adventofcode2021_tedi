﻿using System.Diagnostics;
using System.Threading.Tasks.Sources;
using Day21;
using Shared;

class Program
{
  private static int MaxScore = 1000;
  static void Main()
  {
    Stopwatch sw = new();
    sw.Start();

    FileReader reader = new("input.txt");

    List<string> lines = reader.ReadStringLines();

    List<int> playerPositions = lines.Select(x => x.Split(":")[1].Trim()).Select(int.Parse).ToList();

    Player player1 = new() { Position = playerPositions[0] };
    Player player2 = new() { Position = playerPositions[1] };

    Dice dice = new Dice();

    while (player1.Score < MaxScore && player2.Score < MaxScore)
    {
      player1.Move(dice.Roll(3));
      if (player1.Score >= MaxScore)
      {
        break;
      }
      player2.Move(dice.Roll(3));
    }

    long partOne = dice.DiceRolls * (player1.Score > player2.Score ? player2.Score : player1.Score);
    Part2(playerPositions);

    sw.Stop();
    Console.WriteLine($"Part one: {partOne}, Part two: {0}, Ended in {sw.ElapsedMilliseconds} ms");
    Console.ReadKey();
  }

  private static long Part2(List<int> playerPositions)
  {
    Player player1 = new() { Position = playerPositions[0] };
    Player player2 = new() { Position = playerPositions[1] };

    Universe p1Universe = new(player1.Score);
    Universe p2Universe = new(player2.Score);

    for (int i = 0; i < 16; i++)
    {
      Console.WriteLine($"Depth {i}");
      List<Universe> childUniverses = Enumerable.Range(1, 3).Select(x => new Universe(x)).ToList();
      p1Universe.Inject(childUniverses.Select(u => u.CreateCopy()).ToList(), p1Universe, 1);
      p2Universe.Inject(childUniverses.Select(u => u.CreateCopy()).ToList(), p2Universe, 1);
    }

    (int score, int count) p1Wins = FindChild(p1Universe);
    Console.WriteLine($"P1: {p1Wins.score} - {p1Wins.count}");
    (int score, int count) p2Wins = FindChild(p2Universe);
    Console.WriteLine($"P2: {p2Wins.score} - {p2Wins.count}");

    return 0;

  }

  private static (int score, int count) FindChild(Universe universe)
  {
    int score = 0;
    int count = 0;
    if (universe.ChildUniverses.Any())
    {
      foreach (Universe childUniverse in universe.ChildUniverses)
      {
        (int rScore, int rCount) = FindChild(childUniverse);
        score += rScore;
        count += rCount;
      }

    }
    else
    {
      if(universe.Score >= 10)
      {
        score++;
      }
      count++;
    }
    return (score, count);
  }
}