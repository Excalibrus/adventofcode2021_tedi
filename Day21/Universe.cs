namespace Day21
{
  public class Universe
  {
    private Guid Id { get; set; }
    private readonly long _currentScore;
    public long Score => _currentScore + (ParentUniverse?.Score ?? 0);
    public long Position { get; set; } = 0;
    public List<Universe> ChildUniverses { get; set; } = new();
    public Universe ParentUniverse { get; set; }

    public Universe(long score)
    {
      _currentScore = score;
      Id = Guid.NewGuid();
    }

    public void Inject(List<Universe> universes, Universe parent, int depth)
    {
      // Console.WriteLine($"Depth: {depth}");
      if (ChildUniverses.Count > 0)
      {
        ChildUniverses.ForEach(x => x.Inject(universes, this, depth + 1));
      }
      else
      {
        ChildUniverses = universes.Select(x => x.CreateCopy()).ToList();
        ChildUniverses.ForEach(x => x.ParentUniverse = parent);
      }
    }

    public Universe CreateCopy()
    {
      return new Universe(_currentScore) { Id = Id, ChildUniverses = new() };
    }
  }
}