using System.Drawing;

namespace Day21;

public class Player
{
  public long Score { get; set; } = 0;
  public int Position { get; set; } = 0;

  public void Move(int positions)
  {
    Position += positions;
    if (Position > 10)
    {
      Position = int.Parse(Position.ToString()[^1].ToString());
      if (Position == 0)
      {
        Position = 10;
      }
    }
    Score += Position;
  }

  public override string ToString()
  {
    return $"Score: {Score}, Position: {Position}";
  }
}