﻿using System.Diagnostics;
using Shared;

static class Program
{
  public static int MinSum { get; set; } = int.MaxValue;
  public static List<(int x, int y)> MinPath { get; set; } = new();
  public static (int X, int Y) MatrixSize { get; set; } = (0, 0);
  public static Dictionary<(int x, int y), int> PositionsCache { get; set; } = new();


  public static void Main() {
    Stopwatch sw = new();
    sw.Start();

    FileReader reader = new("input.txt");

    int[,] matrix = reader.ReadIntMatrix();

    // MatrixSize = (matrix.GetLengthX(), matrix.GetLengthY());
    // MinSum = int.MaxValue;
    // PositionsCache = matrix.AllPositions().ToDictionary(x => x, x => MinSum);
    //
    // matrix.AddFirstDiagonalPath();
    // matrix.Go(new List<(int x, int y)>(), (0, 0), 0);
    //
    // DrawPaths(matrix);

    int[,] increasedMatrix = matrix.GetIncreasedMatrix();

    MinPath = new List<(int x, int y)>();
    MinSum = int.MaxValue;
    MatrixSize = (increasedMatrix.GetLengthX(), increasedMatrix.GetLengthY());
    PositionsCache = increasedMatrix.AllPositions().ToDictionary(x => x, x => MinSum);
    increasedMatrix.AddFirstDiagonalPath();
    increasedMatrix.Go(new List<(int x, int y)>(), (0, 0), 0);

    DrawPaths(increasedMatrix);

    sw.Stop();
    Console.WriteLine($"Ended in {sw.ElapsedMilliseconds} ms");
  }

  private static void AddFirstDiagonalPath(this int[,] matrix, bool assignMinSum = true)
  {
    List<(int x, int y)> path = new();
    int sum = 0;
    (int x, int y) position = (0, 0);
    while (position.x < MatrixSize.X && position.y < MatrixSize.Y)
    {
      path.Add(position);
      sum += matrix.GetAtCoordinate(position);
      PositionsCache[position] = sum;
      if (position.x >= position.y)
      {
        position.y++;
      }
      else
      {
        position.x++;
      }
    }
    MinPath = path;
    if (assignMinSum)
    {
      MinSum = sum;
    }

  }

  private static void Go(this int[,] matrix, List<(int x, int y)> path, (int x, int y) position, int sum)
  {
    int value = matrix.GetAtCoordinate(position);
    if (value == 0)
    {
      // cut off
      return;
    }
    if (position.x > 0 || position.y > 0)
    {
      sum += value;
    }
    if (sum >= MinSum)
    {
      return;
    }
    if (PositionsCache[position] <= sum)
    {
      return;
    }
    PositionsCache[position] = sum;
    path.Add(position);
    if (position.x == MatrixSize.X - 1 && position.y == MatrixSize.Y - 1)
    {
      MinSum = sum;
      MinPath = path;
      Console.Write($"\rSum: {sum}  ");
      return;
    }

    List<(int x, int y)> neighbours = position.GetNeighbourCoordinates(
            MatrixPosition.Plus,
            MatrixSize.X,
            MatrixSize.Y)
        .Where(x => !path.Contains(x))
        .ToList();

    if (!neighbours.Any())
    {
      return;
    }

    foreach (((int x, int y) pos, int risk) neighbour in neighbours
                 .Select(pos => (position: pos, risk: matrix.GetAtCoordinate(pos)))
                 .OrderByDescending(x => x.position)
             )
    {
      matrix.Go(path.ToList(), neighbour.pos, sum);
    }
  }

  private static int[,] GetIncreasedMatrix(this int[,] matrix)
  {
    int[,] newMatrix = new int[matrix.GetLength(0) * 5, matrix.GetLength(1) * 5];

    int lengthX = matrix.GetLengthX();
    int lengthY = matrix.GetLengthY();
    matrix.AllPositions().ToList().ForEach(pos =>
    {
      int value = matrix.GetAtCoordinate(pos);
      // copy existing
      newMatrix.SetAtCoordinate(pos, value);
      for (int i = 1; i < 5; i++)
      {
        int newValue = value + i > 9 ? value + i - 9 : value + i;
        // right
        newMatrix.SetAtCoordinate((pos.x + lengthX * i, pos.y), newValue);
        // down
        newMatrix.SetAtCoordinate((pos.x, pos.y + lengthY * i), newValue);

        // new right items down
        for (int j = 1; j < 5; j++)
        {
          int newValueDown = newValue + j > 9 ? newValue + j - 9 : newValue + j;
          newMatrix.SetAtCoordinate((pos.x + lengthX * i, pos.y + lengthY * j), newValueDown);
        }
      }
    });

    int leftY = newMatrix.GetLengthY() / 3;
    (int x, int y) leftStartingPoint = (0, leftY);
    while (leftStartingPoint.y < newMatrix.GetLengthY())
    {
      for (int y = leftStartingPoint.y; y < newMatrix.GetLengthY(); y++)
      {
        newMatrix.SetAtCoordinate((leftStartingPoint.x,y), 0);
      }
      leftStartingPoint = (leftStartingPoint.x + 1, leftStartingPoint.y + 1);
    }

    int topX = newMatrix.GetLengthX() / 3;
    (int x, int y) topStartingPoint = (topX, 0);
    while (topStartingPoint.x < newMatrix.GetLengthX())
    {
      for (int y = 0; y < topStartingPoint.x - topX; y++)
      {
        newMatrix.SetAtCoordinate((topStartingPoint.x,y), 0);
      }
      topStartingPoint = (topStartingPoint.x + 1, topStartingPoint.y + 1);
    }
    //
    // // cutting left bottom
    // int halfX = newMatrix.GetLengthX() / 2;
    // int halfY = newMatrix.GetLengthY() / 2;
    // for (int x = 0; x < halfX; x++)
    // {
    //   for (int y = halfY + x; y < newMatrix.GetLengthY(); y++)
    //   {
    //       newMatrix.SetAtCoordinate((x,y), 0);
    //   }
    // }
    // // cutting top right
    // for (int x = halfX; x < newMatrix.GetLengthX(); x++)
    // {
    //   for (int y = 0; y < x - halfY; y++)
    //   {
    //     newMatrix.SetAtCoordinate((x,y), 0);
    //   }
    // }

    return newMatrix;
  }

  private static void DrawPaths(int[,] matrix)
  {
    Console.WriteLine();

    Console.WriteLine($"Sum: {MinSum}");

    foreach ((int x, int y) position in matrix.AllPositions(true))
    {
      if (MinPath.Contains(position))
      {
        Console.ForegroundColor = ConsoleColor.Red;
      }
      Console.Write(matrix.GetAtCoordinate(position));
      if (MinPath.Contains(position))
      {
        Console.ResetColor();
      }
    }
    Console.WriteLine();
  }
}
