using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Day05
{
  public class VentLine
  {
    public int X1 { get; set; }
    public int Y1 { get; set; }
    public int X2 { get; set; }
    public int Y2 { get; set; }

    public int MaxCor = 0;

    public VentLine(string line)
    {
      List<string> list = line.Split(" -> ").Select(x => x.Trim()).ToList();
      if (list.Count == 2)
      {
        List<string> firstCor = list[0].Split(",").Select(x => x.Trim()).ToList();
        if (firstCor.Count == 2)
        {
          X1 = int.Parse(firstCor[0]);
          Y1 = int.Parse(firstCor[1]);
        }
        List<string> secondCor = list[1].Split(",").Select(x => x.Trim()).ToList();
        if (secondCor.Count == 2)
        {
          X2 = int.Parse(secondCor[0]);
          Y2 = int.Parse(secondCor[1]);
        }
      }
      MaxCor = new[] {X1, Y1, X2, Y2}.Max();
    }

  }
}