﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Shared;

namespace Day05
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Hello World!");

      FileReader reader = new("input.txt");

      List<string> fileLines = reader.ReadStringLines();

      List<VentLine> ventLines = fileLines.Select(x => new VentLine(x)).ToList();

      int max = ventLines.Max(x => x.MaxCor);
      int[,] matrix = new int[max + 1, max + 1];
      for (int i = 0; i <= max; i++)
      {
        for (int j = 0; j <= max; j++)
        {
          matrix[i, j] = 0;
        }
      }

      foreach (VentLine ventLine in ventLines)
      {
        FillMatrix(matrix, ventLine);
      }

      // for (int i = 0; i <= max; i++)
      // {
      //   for (int j = 0; j <= max; j++)
      //   {
      //     Console.Write(matrix[j,i] > 0 ? matrix[j, i].ToString() : ".");
      //   }
      //   Console.Write("\n");
      // }

      int intersections = CountIntersections(matrix);

      Console.WriteLine($"Result 1: {intersections}");

      Console.ReadKey();

    }

    private static void FillMatrix(int[,] matrix, VentLine ventLine)
    {
      // vertical
      if (ventLine.X1 == ventLine.X2)
      {
        if (ventLine.Y1 > ventLine.Y2)
        {
          for (int y = ventLine.Y2; y <= ventLine.Y1; y++)
          {
            matrix[ventLine.X1, y]++;
          }
        }
        else
        {
          for (int y = ventLine.Y1; y <= ventLine.Y2; y++)
          {
            matrix[ventLine.X1, y]++;
          }
        }
      }
      // horizontal
      else if (ventLine.Y1 == ventLine.Y2)
      {
        if (ventLine.X1 > ventLine.X2)
        {
          for (int x = ventLine.X2; x <= ventLine.X1; x++)
          {
            matrix[x, ventLine.Y1]++;
          }
        }
        else
        {
          for (int x = ventLine.X1; x <= ventLine.X2; x++)
          {
            matrix[x, ventLine.Y1]++;
          }
        }
      }
      // diagonal
      else
      {
        if (ventLine.X1 > ventLine.X2)
        {
          for (int x = ventLine.X2, y = ventLine.Y2; x <= ventLine.X1 && (ventLine.Y2 > ventLine.Y1 ? ventLine.Y1 <= y : ventLine.Y1 >= y); x++, y = (ventLine.Y2 > ventLine.Y1 ? y - 1 : y + 1))
          {
            matrix[x, y]++;
          }
        }
        else
        {
          for (int x = ventLine.X1, y = ventLine.Y1; x <= ventLine.X2 && (ventLine.Y1 > ventLine.Y2 ? ventLine.Y2 <= y : ventLine.Y2 >= y); x++, y = (ventLine.Y1 > ventLine.Y2 ? y - 1 : y + 1))
          {
            matrix[x, y]++;
          }
        }
      }
    }

    private static int CountIntersections(int[,] matrix)
    {
      int counter = 0;
      for (int x = 0; x < matrix.GetLength(0); x++)
      {
        for (int y = 0; y < matrix.GetLength(1); y++)
        {
          if (matrix[x, y] > 1)
          {
            counter++;
          }
        }
      }
      return counter;
    }
  }
}