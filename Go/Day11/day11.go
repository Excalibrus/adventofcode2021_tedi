package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
)

func main() {
	absPath, _ := filepath.Abs("input_demo.txt")
	file, err := os.Open(absPath)

	if err != nil {
		log.Fatalln("failed to open", err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	matrix := [][]int{}

	for scanner.Scan() {
		matrixRow := []int{}
		line := scanner.Text()
		for _, c := range line {
			item, _ := strconv.Atoi(string(c))
			matrixRow = append(matrixRow, item)
		}
		matrix = append(matrix, matrixRow)
	}

	draw(matrix)
}

func draw(matrix [][]int) {
	for x, _ := range matrix {
		for y, _ := range matrix[x] {
			fmt.Print(matrix[x][y])
		}
		fmt.Println()
	}
}
