﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Shared;

namespace Day10
{
  class Program
  {
    static void Main(string[] args)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();

      FileReader reader = new("input.txt");

      List<string> lines = reader.ReadStringLines();

      List<string> validCharacters = new() { "()", "{}", "[]", "<>" };
      List<string> incorrectCharacters = new();
      List<long> p2Scores = new();
      for (int i = 0; i < lines.Count; i++)
      {
        string chunk = lines[i];
        bool validCharNotFound = false;
        while (validCharacters.Any(x => chunk.Contains(x)))
        {
          foreach (string validCharacter in validCharacters)
          {
            int index = chunk.IndexOf(validCharacter);
            if (index > -1)
            {
              chunk = chunk.Remove(index, 2);
              lines[i] = chunk;
            }
          }
        }

        char invalidCharacter = chunk.FirstOrDefault(x => new[] { ")", "}", "]", ">" }.Contains(x.ToString()));
        if (invalidCharacter != 0)
        {
          incorrectCharacters.Add(invalidCharacter.ToString());
        }
        else
        {
          int ReplaceCharsFunction(char x)
          {
            if (x == '{') return '}';
            if (x == '(') return ')';
            if (x == '[') return ']';
            if (x == '<') return '>';
            return 0;
          }

          List<int> charsToCalculate = chunk.Reverse().Select((Func<char, int>)ReplaceCharsFunction).ToList();
          long total = 0;
          foreach (int charCalc in charsToCalculate)
          {
            total *= 5;
            if (charCalc == ')') total += 1;
            if (charCalc == ']') total += 2;
            if (charCalc == '}') total += 3;
            if (charCalc == '>') total += 4;
          }
          p2Scores.Add(total);
        }
        // if(new[] { ")", "}", "]", ">" }.Contains(chunk.TakeLast(1).ToString()))
        // {
        //   incorrectCharacters.Add(chunk.TakeLast(1).ToString());
        // }
      }

      int SumFunction(string x)
      {
        if (x == ")") return 3;
        if (x == "]") return 57;
        if (x == "}") return 1197;
        if (x == ">") return 25137;
        return 0;
      }
      int sumP1 = incorrectCharacters.Sum((Func<string,int>)SumFunction);

      sw.Stop();

      int scoreIndex = (int)Math.Round((decimal)p2Scores.Count / 2, MidpointRounding.ToZero);
      long p2Score = p2Scores.OrderBy(x => x).ToList()[scoreIndex];

      Console.WriteLine($"Result1: {sumP1}, Result 2: {p2Score}, Time: {sw.ElapsedMilliseconds} ms");
    }
  }
}