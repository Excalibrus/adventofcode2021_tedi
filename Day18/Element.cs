using System.Text;

namespace Day18;

public class Element
{
  public Guid Id { get; set; } = Guid.NewGuid();
  public int Hierarchy { get; set; } = 0;
  public int XIterationIndex { get; set; } = 0;
  public int YIterationIndex { get; set; } = 0;
  public Element? Parent { get; set; }
  public int? XNumber { get; set; }
  public int? YNumber { get; set; }
  public Element? XElement { get; set; }
  public Element? YElement { get; set; }

  public int? Magnitude => XMagnitude + YMagnitude;
  public int? XMagnitude => 3 * (IsXNumber ? XNumber : XElement?.Magnitude);
  public int? YMagnitude => 2 * (IsYNumber ? YNumber : YElement?.Magnitude);

  public bool IsXNumber
  {
    get
    {
      if (XNumber.HasValue && XElement is null)
      {
        return true;
      }
      if (!XNumber.HasValue && XElement != null)
      {
        return false;
      }
      throw new Exception($"Could not get format for X element with ID {Id.ToString()}");
    }
  }

  public bool IsYNumber
  {
    get
    {
      if (YNumber.HasValue && YElement is null)
      {
        return true;
      }
      if (!YNumber.HasValue && YElement != null)
      {
        return false;
      }
      throw new Exception($"Could not get format for Y element with ID {Id.ToString()}");
    }
  }

  public override string ToString()
  {
    StringBuilder sb = new();
    sb.Append('[');
    if (IsXNumber)
    {
      sb.Append($"{XNumber},");
    }
    else
    {
      sb.Append(XElement);
      sb.Append(',');
    }
    if (IsYNumber)
    {
      sb.Append($"{YNumber}");
    }
    else
    {
      sb.Append(YElement);
    }
    sb.Append(']');
    return sb.ToString();
  }

  public Element CreateCopy(Element? parent = null)
  {
    Element element = new Element
    {
        Hierarchy = Hierarchy,
        XNumber = XNumber,
        YNumber = YNumber,
        XIterationIndex = XIterationIndex,
        YIterationIndex = YIterationIndex,
        Parent = parent
    };
    element.XElement = XElement?.CreateCopy(element);
    element.YElement = YElement?.CreateCopy(element);
    return element;
  }

}