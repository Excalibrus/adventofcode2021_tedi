﻿using System.Diagnostics;
using Shared;

namespace Day18;

public static class Program
{
  static void Main()
  {
    RunTests();

    Stopwatch sw = new();
    sw.Start();

    FileReader reader = new("input.txt");

    List<string> lines = reader.ReadStringLines();

    List<Element> elements = new();
    foreach (string line in lines)
    {
      (Element element, _) = line.ParseElement();
      element.ApplyHierarchy();
      elements.Add(element);
    }

    Element finalElement = elements[0].CreateCopy();
    for (int i = 1; i < elements.Count; i++)
    {
      finalElement = DoAddition(finalElement, elements[i].CreateCopy());
    }

    int partTwo = elements.PartTwo();

    // Console.WriteLine(finalElement);

    sw.Stop();
    Console.WriteLine($"Part one: {finalElement.Magnitude}, Part two: {partTwo}, Ended in {sw.ElapsedMilliseconds} ms");
    Console.ReadKey();
  }

  private static int PartTwo(this List<Element> elements)
  {
    int maxMagnitude = 0;
    Element el1 = null;
    Element el2 = null;
    Element maxEl = null;
    for (int i = 0; i < elements.Count; i++)
    {
      for (int j = elements.Count - 1; j >= 0; j--)
      {
        if (i != j)
        {
          Element el1Copy = elements[i].CreateCopy();
          Element el2Copy = elements[j].CreateCopy();
          Element element = DoAddition(el1Copy, el2Copy);
          if (element.Magnitude.HasValue && element.Magnitude > maxMagnitude)
          {
            maxMagnitude = element.Magnitude.Value;
            el1 = el1Copy;
            el2 = el2Copy;
            maxEl = element;
          }
        }
      }
    }
    Console.WriteLine(el1);
    Console.WriteLine(el2);
    Console.WriteLine(maxEl);
    return maxMagnitude;
  }

  private static Element DoAddition(Element element1, Element element2)
  {
    // Console.WriteLine("  " + element1);
    // Console.WriteLine("+ " + element2);
    Element element = element1.Concat(element2);
    element.ApplyHierarchy();
    Element? elementToExplode = element.GetElementToExplode();
    (Element? elementToSplit, string position) split = element.GetElementToSplit();
    int steps = 0;
    while (elementToExplode != null || split.elementToSplit != null)
    {
      steps++;
      if (elementToExplode != null)
      {
        element.ExplodeChildElement(elementToExplode);
        element.ApplyHierarchy();
        // Console.WriteLine($"After explode: {element}");
      }
      else if (split.elementToSplit != null)
      {
        split.elementToSplit.SplitElement(split.position);
        element.ApplyHierarchy();
        // Console.WriteLine($"After split:   {element}");

      }
      elementToExplode = element.GetElementToExplode();
      split = element.GetElementToSplit();
    }

    // Console.WriteLine($"Steps {steps}");

    // Console.WriteLine("= " + element);
    // Console.WriteLine("\n\n");
    return element;
  }

  private static (Element element, int index) ParseElement(this string line)
  {
    if (line[0] == '[' && line[1] == '[')
    {
      Element element = new();
      int index = 1;
      (Element xElement, int xIndex) = line[1..].ParseElement();
      index = xElement.ToString().Length + 1;
      xElement.Parent = element;
      element.XElement = xElement;
      if (line[index] != ',') throw new Exception();
      if (line[index + 1] == '[')
      {
        (Element? yElement, int yIndex) = line[(index + 1)..].ParseElement();
        yElement.Parent = element;
        index += yIndex;
        element.YElement = yElement;
      }
      else if (int.TryParse(line[index + 1].ToString(), out int numY))
      {
        element.YNumber = numY;
        if (line[index + 2] != ']') throw new Exception();
        index += 2;
      }
      else throw new Exception();
      return (element, index);
    }
    if (line[0] == '[' && int.TryParse(line[1].ToString(), out int numX))
    {
      Element element = new() { XNumber = numX };
      int index = 0;
      if (line[2] != ',') throw new Exception();
      if (line[3] == '[')
      {
        (Element? yElement, int yIndex) = line[3..].ParseElement();
        index += yIndex;
        if (yElement is null) throw new Exception();
        yElement.Parent = element;
        element.YElement = yElement;
      }
      else if (int.TryParse(line[3].ToString(), out int numY))
      {
        element.YNumber = numY;
        if (line[4] != ']') throw new Exception();
        index += 4;
      }
      else throw new Exception("Parsing error");
      return (element, index);
    }
    throw new Exception();
  }

  public static Element Concat(this Element xElement, Element yElement)
  {
    Element element = new() { XElement = xElement, YElement = yElement };
    xElement.Parent = element;
    yElement.Parent = element;
    return element;
  }

  private static int ApplyHierarchy(this Element element, int startingNumber = 1, int iterationIndex = 1)
  {
    element.Hierarchy = startingNumber;
    element.XIterationIndex = 0;
    element.YIterationIndex = 0;
    if (!element.IsXNumber)
    {
      iterationIndex = element.XElement?.ApplyHierarchy(startingNumber + 1, iterationIndex) ?? 0;
    }
    else
    {
      element.XIterationIndex = iterationIndex++;
    }
    if (!element.IsYNumber)
    {
      iterationIndex = element.YElement?.ApplyHierarchy(startingNumber + 1, iterationIndex) ?? 0;
    }
    else
    {
      element.YIterationIndex = iterationIndex++;
    }
    return iterationIndex;
  }

  private static Element? GetElementToExplode(this Element element)
  {
    Element? elementSearch = null;
    if (element.Hierarchy == 5 && element.IsXNumber && element.IsYNumber)
    {
      return element;
    }
    if (!element.IsXNumber)
    {
      elementSearch = element.XElement?.GetElementToExplode();
    }
    if (!element.IsYNumber && elementSearch is null)
    {
      elementSearch = element.YElement?.GetElementToExplode();
    }
    return elementSearch;
  }

  private static void ExplodeChildElement(this Element element, Element childElement)
  {
    // Console.WriteLine($"Exploding element {childElement}");
    if (childElement.Parent == null) throw new Exception("Missing prent");

    (Element? rightElement, string rightPosition) = element.GetElementWithIterationIndex(childElement.YIterationIndex + 1);
    if (rightElement != null)
    {
      if (rightPosition == "x")
      {
        rightElement.XNumber += childElement.YNumber;
      }
      else if (rightPosition == "y")
      {
        rightElement.YNumber += childElement.YNumber;
      }
    }
    (Element? leftElement, string leftPosition) = element.GetElementWithIterationIndex(childElement.XIterationIndex - 1);
    if (leftElement != null)
    {
      if (leftPosition == "x")
      {
        leftElement.XNumber += childElement.XNumber;
      }
      else if (leftPosition == "y")
      {
        leftElement.YNumber += childElement.XNumber;
      }
    }

    if (!childElement.Parent.IsXNumber)
    {
      childElement.Parent.XNumber = 0;
      childElement.Parent.XElement = null;
    }
    else if (!childElement.Parent.IsYNumber)
    {
      childElement.Parent.YNumber = 0;
      childElement.Parent.YElement = null;
    }

    element.ApplyHierarchy();
  }

  private static (Element? element, string position) GetElementToSplit(this Element element)
  {
    (Element? searchElement, string searchPosition, _) = element.GetElementsToSplit().OrderBy(x => x.index).FirstOrDefault();
    return (searchElement, searchPosition);
  }
  private static List<(Element? element, string position, int index)> GetElementsToSplit(this Element element)
  {
    List<(Element? element, string position, int index)> search = new();
    if (element.IsXNumber && element.XNumber >= 10)
    {
      search.Add((element, "x", element.XIterationIndex));
    }
    if (element.IsYNumber && element.YNumber >= 10)
    {
      search.Add((element, "y", element.YIterationIndex));
    }
    if (!element.IsXNumber)
    {
      search.AddRange(element.XElement.GetElementsToSplit());
    }
    if (!element.IsYNumber)
    {
      search.AddRange(element.YElement.GetElementsToSplit());
    }
    return search;
  }

  private static void SplitElement(this Element element, string position)
  {
    // Console.WriteLine($"Splitting element {element}");
    if (position == "x" && element.IsXNumber)
    {
      int newX = Convert.ToInt32(Math.Floor(element.XNumber.Value / 2m));
      int newy = Convert.ToInt32(Math.Ceiling(element.XNumber.Value / 2m));
      element.XNumber = null;
      element.XElement = new Element { XNumber = newX, YNumber = newy, Parent = element};
    }
    else if (position == "y" && element.IsYNumber)
    {
      int newX = Convert.ToInt32(Math.Floor(element.YNumber.Value / 2m));
      int newy = Convert.ToInt32(Math.Ceiling(element.YNumber.Value / 2m));
      element.YNumber = null;
      element.YElement = new Element { XNumber = newX, YNumber = newy, Parent = element};
    }
  }

  private static (Element? element, string position) GetElementWithIterationIndex(this Element element, int index)
  {
    (Element? elementSearch, string positionSearch) search = (null, "");
    if (index == 0) return search;
    if (element.XIterationIndex == index)
    {
      return (element, "x");
    }
    if (element.YIterationIndex == index)
    {
      return (element, "y");
    }
    if (!element.IsXNumber)
    {
      search = element.XElement.GetElementWithIterationIndex(index);
    }
    if (!element.IsYNumber && search.elementSearch is null)
    {
      search = element.YElement.GetElementWithIterationIndex(index);
    }

    return search;
  }

  private static void RunTests()
  {
    "[[[[[4,5],[2,6]],2],3],1]".TestExplosion("[[[[0,[7,6]],2],3],1]");
    "[[[[[9,8],1],2],3],4]".TestExplosion("[[[[0,9],2],3],4]");
    "[7,[6,[5,[4,[3,2]]]]]".TestExplosion("[7,[6,[5,[7,0]]]]");
    "[[6,[5,[4,[3,2]]]],1]".TestExplosion("[[6,[5,[7,0]]],3]");
    "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]".TestExplosion("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]");
    "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]".TestExplosion("[[3,[2,[8,0]]],[9,[5,[7,0]]]]");

    TestAddition("[[[[4,3],4],4],[7,[[8,4],9]]]", "[1,1]", "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]")
        .TestExplosion("[[[[0,7],4],[7,[[8,4],9]]],[1,1]]")
        .TestExplosion("[[[[0,7],4],[15,[0,13]]],[1,1]]")
        .TestSplit("[[[[0,7],4],[[7,8],[0,13]]],[1,1]]")
        .TestSplit("[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]")
        .TestExplosion("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]");
    // "[[[[0,7],4],[15,[0,13]]],[1,1]]".TestSplit("[[[[0,7],4],[[7,8],[0,13]]],[1,1]]");
  }

  private static void CombinedTest1(this string input)
  {
    // TestAddition()
  }

  private static Element TestAddition(string input1, string input2, string expectedString)
  {
    (Element element1, _) = input1.ParseElement();
    (Element element2, _) = input2.ParseElement();

    Element element = element1.Concat(element2);
    if (element is null) throw new Exception();
    element.ApplyHierarchy();

    if (expectedString != element.ToString())
    {
      throw new Exception();
    }
    return element;
  }

  private static Element TestExplosion(this string input, string expectedString)
  {
    (Element element, _) = input.ParseElement();
    return element.TestExplosion(expectedString);
  }

  private static Element TestExplosion(this Element element, string expectedString)
  {
    element.ApplyHierarchy();
    Element? elementToExplode = element.GetElementToExplode();
    if (elementToExplode is null) throw new Exception();
    element.ExplodeChildElement(elementToExplode);
    string explodedString = element.ToString();
    if (expectedString != explodedString)
    {
      throw new Exception();
    }
    return element;
  }

  private static Element TestSplit(this string input, string expectedString)
  {
    (Element element, _) = input.ParseElement();
    return element.TestSplit(expectedString);
  }

  private static Element TestSplit(this Element element, string expectedString)
  {
    element.ApplyHierarchy();
    (Element? elementToSplit, string? position) = element.GetElementToSplit();
    if (elementToSplit is null) throw new Exception();
    elementToSplit.SplitElement(position);
    string splittedString = element.ToString();
    if (expectedString != splittedString)
    {
      throw new Exception();
    }
    return element;
  }


}