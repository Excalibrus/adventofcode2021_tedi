using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Shared;

public static class MatrixExtensions
{
  public static int GetAtCoordinate(this int[,] matrix, (int x, int y) position)
  {
    return matrix[position.y, position.x];
  }

  public static void SetAtCoordinate(this int[,] matrix, (int x, int y) position, int value)
  {
    matrix[position.y, position.x] = value;
  }

  public static int GetLengthX(this int[,] matrix)
  {
    return matrix.GetLength(1);
  }

  public static int GetLengthY(this int[,] matrix)
  {
    return matrix.GetLength(0);
  }

  public static IEnumerable<(int x, int y)> AllPositions(this int[,] matrix, bool writeNewLineAfterEachY = false)
  {
    for (int y = 0; y < matrix.GetLengthY(); y++)
    {
      for (int x = 0; x < matrix.GetLengthX(); x++)
      {
        yield return (x, y);
      }
      if(writeNewLineAfterEachY) Console.WriteLine();
    }
  }

  public static List<(int x, int y)> GetNeighbourCoordinates(this (int x, int y) coordinate, MatrixPosition position, int maxX, int maxY)
  {
    (int x, int y) = coordinate;
    List<(int x, int y)> coordinates = new();
    if (x > 0)
    {
      if (position.HasFlag(MatrixPosition.Left))
      {
        coordinates.Add((x - 1, y));
      }
      if (y > 0 && position.HasFlag(MatrixPosition.TopLeft))
      {
        coordinates.Add((x - 1, y - 1));
      }
      if (y < maxY - 1 && position.HasFlag(MatrixPosition.BottomLeft))
      {
        coordinates.Add((x - 1, y + 1));
      }
    }
    if (x < maxX - 1)
    {
      if (position.HasFlag(MatrixPosition.Right))
      {
        coordinates.Add((x + 1, y));
      }
      if (y > 0 && position.HasFlag(MatrixPosition.TopRight))
      {
        coordinates.Add((x + 1, y - 1));
      }
      if (y < maxY - 1 && position.HasFlag(MatrixPosition.BottomRight))
      {
        coordinates.Add((x + 1, y + 1));
      }
    }
    if (y > 0 && position.HasFlag(MatrixPosition.Top))
    {
      coordinates.Add((x, y - 1));
    }
    if (y < maxY - 1 && position.HasFlag(MatrixPosition.Bottom))
    {
      coordinates.Add((x, y + 1));
    }
    return coordinates;
  }

  public static (int x, int y)? GetNeighbourCoordinate(this (int x, int y) coordinate, MatrixPosition position, int maxX, int maxY)
  {
    List<(int x, int y)> neighbourCoordinates = GetNeighbourCoordinates(coordinate, position, maxX, maxY);
    return neighbourCoordinates.Count == 1 ? neighbourCoordinates.First() : null;
  }
}