using System;

namespace Shared;

[Flags]
public enum MatrixPosition
{
  Left = 1 << 0,
  Right = 1 << 1,
  Top = 1 << 2,
  Bottom = 1 << 3,
  TopLeft = 1 << 4,
  TopRight = 1 << 5,
  BottomLeft = 1 << 6,
  BottomRight = 1 << 7,
  Horizontal = Left | Right,
  Vertical = Top | Bottom,
  Diagonal = TopLeft | TopRight | BottomLeft | BottomRight,
  Plus = Vertical | Horizontal,
  All = Plus | Diagonal
}