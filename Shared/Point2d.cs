using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared;

public class Point2d
{
  public int X { get; set; }
  public int Y { get; set; }

  public Point2d()
  {

  }

  public Point2d(int x, int y)
  {
    X = x;
    Y = y;
  }

  public Point2d Parse(string line)
  {
    List<string> items = line.Split(",").ToList();
    if (items.Count != 2)
    {
      throw new Exception($"Failed to parse 2d point {line}");
    }
    X = int.Parse(items[0]);
    Y = int.Parse(items[1]);
    return this;
  }

  public override string ToString()
  {
    return $"{X},{Y}";
  }

  public override bool Equals(object obj)
  {
    if (obj is Point2d point)
    {
      return point.X == X && point.Y == Y;
    }
    return base.Equals(obj);
  }

  public override int GetHashCode()
  {
    return HashCode.Combine(X, Y);
  }
}