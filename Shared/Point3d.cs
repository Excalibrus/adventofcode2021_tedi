using System;
using System.Collections.Generic;
using System.Linq;

namespace Shared;

public class Point3d : IEqualityComparer<Point3d>, IEquatable<Point3d>
{
  public int X { get; set; }
  public int Y { get; set; }
  public int Z { get; set; }

  public Point3d()
  {

  }

  public Point3d(int x, int y, int z)
  {
    X = x;
    Y = y;
    Z = z;
  }

  public Point3d Parse(string line)
  {
    List<string> items = line.Split(",").ToList();
    if (items.Count != 3)
    {
      throw new Exception($"Failed to parse 2d point {line}");
    }
    X = int.Parse(items[0]);
    Y = int.Parse(items[1]);
    Z = int.Parse(items[2]);
    return this;
  }

  public static Point3d operator -(Point3d a, Point3d b)
    => new()
    {
        X = a.X - b.X,
        Y = a.Y - b.Y,
        Z = a.Z - b.Z
    };

  public static Point3d operator +(Point3d a, Point3d b)
    => new()
    {
        X = a.X + b.X,
        Y = a.Y + b.Y,
        Z = a.Z + b.Z
    };

  public override string ToString()
  {
    return $"{X},{Y},{Z}";
  }

  public override bool Equals(object obj)
  {
    if (obj is Point3d point)
    {
      // return Math.Abs(point.X) == Math.Abs(X) && Math.Abs(point.Y) == Math.Abs(Y) && Math.Abs(point.Z) == Math.Abs(Z);
      // return point.X == X && point.Y == Y && point.Z == Z;
      return Equals(point, this);
    }
    return base.Equals(obj);
  }

  public override int GetHashCode()
  {
    return HashCode.Combine(X, Y, Z);
  }

  public Point3d RotatePoint(int direction)
  {
    return direction switch
    {
        0 => new Point3d(X,Y,Z),
        1 => new Point3d(X, -Z, Y),
        2 => new Point3d(X, -Y, -Z),
        3 => new Point3d(X, Z, -Y),
        4 => new Point3d(-Y, X, Z),
        5 => new Point3d(Z, X, Y),
        6 => new Point3d(Y, X, -Z),
        7 => new Point3d(-Z, X, -Y),
        8 => new Point3d(-X, -Y, Z),
        9 => new Point3d(-X, -Z, -Y),
        10 => new Point3d(-X, Y, -Z),
        11 => new Point3d(-X, Z, Y),
        12 => new Point3d(Y, -X, Z),
        13 => new Point3d(Z, -X, -Y),
        14 => new Point3d(-Y, -X, -Z),
        15 => new Point3d(-Z, -X, Y),
        16 => new Point3d(-Z, Y, X),
        17 => new Point3d(Y, Z, X),
        18 => new Point3d(Z, -Y, X),
        19 => new Point3d(-Y, -Z, X),
        20 => new Point3d(-Z, -Y, -X),
        21 => new Point3d(-Y, Z, -X),
        22 => new Point3d(Z, Y, -X),
        23 => new Point3d(Y, -Z, -X),
        _ => throw new Exception("Invalid rotation")
    };
  }


  public bool Equals(Point3d? point, Point3d? comparingPoint)
  {
    return point != null && comparingPoint != null && point.X == comparingPoint.X && point.Y == comparingPoint.Y && point.Z == comparingPoint.Z;
  }

  public int GetHashCode(Point3d obj)
  {
    return HashCode.Combine(X, Y, Z);
  }
  public bool Equals(Point3d other)
  {
    if (ReferenceEquals(null, other)) return false;
    if (ReferenceEquals(this, other)) return true;
    return X == other.X && Y == other.Y && Z == other.Z;
  }
}