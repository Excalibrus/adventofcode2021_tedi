namespace Day06
{
  public class Fish
  {
    public int Number { get; set; }
    public long Repetitions { get; set; }
  }
}