﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Shared;

namespace Day06
{
  class Program
  {
    static void Main(string[] args)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();

      FileReader reader = new("input.txt");

      string line = reader.ReadStringLines().FirstOrDefault();

      List<int> lines = line?.Split(",").Select(int.Parse).ToList();
      if (lines == null) throw new Exception("No fish");

      List<Fish> fish = new();
      foreach (int fishNum in lines)
      {
        Fish fishy = fish.FirstOrDefault(x => x.Number == fishNum);
        if (fishy == null)
        {
          fish.Add(new Fish{ Number = fishNum, Repetitions = 1});
        }
        else
        {
          fishy.Repetitions++;
        }
      }
      int days = 256;
      int mergeThreshold = 5;
      for (int j = 1; j <= days; j++)
      {
        int dailyCount = fish.Count;
        for (int i = 0; i < dailyCount; i++)
        {
          Fish fishy = fish[i];
          if (fishy.Number > 0)
          {
            fishy.Number--;
          }
          else
          {
            fish.Add(new Fish {Number = 8, Repetitions = fishy.Repetitions});
            
            fishy.Number = 6;
          }
        }
        if (j % mergeThreshold == 0)
        {
          fish = MergeExistingFish(fish);
        }
        // Console.WriteLine($"Day {j} finished, daily count: {dailyCount}");
      }
      long result = fish.Sum(x => x.Repetitions);

      sw.Stop();

      Console.WriteLine($"Result 1: {result}, time: {sw.ElapsedMilliseconds} ms");
    }

    private static List<Fish> MergeExistingFish(List<Fish> fish)
    {
      List<Fish> mergedList = new();
      for (int i = 0; i <= 8; i++)
      {
        long repetitions = fish.Where(x => x.Number == i).Sum(x => x.Repetitions);
        if (repetitions > 0)
        {
          mergedList.Add(new Fish{ Number = i, Repetitions = repetitions});
        }
      }
      return mergedList;
    }
  }
}