﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Shared;

namespace Day09
{
  class Program
  {
    static void Main(string[] args)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();

      FileReader reader = new("input.txt");

      List<string> lines = reader.ReadStringLines();

      int[,] matrix = new int[lines.Count, lines[0].Length];

      for (int x = 0; x < lines.Count; x++)
      {
        string line = lines[x];
        for (int y = 0; y < line.Length; y++)
        {
          int number = int.Parse(line[y].ToString());
          matrix[x, y] = number;
        }
      }

      int sum = 0;
      // for (int x = 0; x < matrix.GetLength(0); x++)
      // {
      //   for (int y = 0; y < matrix.GetLength(1); y++)
      //   {
      //     int up = -1;
      //     int down = -1;
      //     int left = -1;
      //     int right = -1;
      //     if (x > 0) left = matrix[x - 1, y];
      //     if (x < matrix.GetLength(0) - 1) right = matrix[x + 1, y];
      //     if (y > 0) up = matrix[x, y - 1];
      //     if (y < matrix.GetLength(1) - 1) down = matrix[x, y + 1];
      //
      //     int min = new[] {up, down, left, right}.Where(x => x >= 0).Min();
      //     if (min > matrix[x, y])
      //     {
      //       sum += matrix[x, y] + 1;
      //     }
      //   }
      // }

      int result2 = PartTwo(matrix);

      sw.Stop();

      Console.WriteLine($"Result2: {result2}, Time: {sw.ElapsedMilliseconds} ms");
    }

    private static int PartTwo(int[,] matrix)
    {
      List<Dictionary<(int x, int y), int>> allBasins = new();
      List<(int x, int y)> usedPoints = new();
      for (int x = 0; x < matrix.GetLength(0); x++)
      {
        for (int y = 0; y < matrix.GetLength(1); y++)
        {
          Dictionary<(int x, int y), int> currentBasin = new();
          GetAllBasinPoints(matrix, x, y, currentBasin, usedPoints);
          if (currentBasin.Any())
          {
            allBasins.Add(currentBasin);
          }
        }
      }
      List<Dictionary<(int x, int y),int>> largestBasins = allBasins.OrderBy(x => x.Count).TakeLast(3).ToList();

      return largestBasins.Select(x => x.Count).Aggregate((x, y) => x * y);
    }

    private static void GetAllBasinPoints(int[,] matrix, int x, int y, Dictionary<(int x, int y), int> currentBasin, List<(int x, int y)> usedPoints)
    {
      if (matrix[x, y] == 9) return;
      if(!currentBasin.ContainsKey((x,y)) && !usedPoints.Any(point => point.x == x && point.y == y))
      {
        currentBasin.Add((x,y), matrix[x,y]);
        usedPoints.Add((x,y));
      }
      List<(int x, int y)> neighbours = GetNeighbours(x, y, matrix);
      foreach ((int x, int y) neighbour in neighbours)
      {
        if (matrix[neighbour.x, neighbour.y] != 9 && !currentBasin.ContainsKey((neighbour.x, neighbour.y)) && !usedPoints.Any(point => point.x == neighbour.x && point.y == neighbour.y))
        {
          GetAllBasinPoints(matrix, neighbour.x, neighbour.y, currentBasin, usedPoints);
        }
      }
    }

    private static List<(int x, int y)> GetNeighbours(int x, int y, int[,] matrix)
    {
      int xLength = matrix.GetLength(0);
      int yLength = matrix.GetLength(1);
      List<(int x, int y)> coordinates = new();
      if (x > 0) coordinates.Add((x - 1, y));
      if (x < xLength - 1) coordinates.Add((x + 1, y));
      if (y > 0) coordinates.Add((x, y - 1));
      if (y < yLength - 1) coordinates.Add((x, y + 1));
      return coordinates;
    }
  }


}